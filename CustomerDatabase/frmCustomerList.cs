﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace CustomerDatabase
{
    public partial class frmCustomerList : Form
    {
        // Store customer information
        private Customer customer;

        // Store how many delete check boxes are checked
        private int checkCounts;

        // Store boolean for determining if the form is initialized
        private bool formInitialized;

        public frmCustomerList()
        {
            InitializeComponent();
        }

        private void frmCustomerList_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the '_AHDatabase_CustomerContextDataSet.Customers' table. You can move, or remove it, as needed.
            this.customersTableAdapter.Fill(this._AHDatabase_CustomerContextDataSet.Customers);
            formInitialized = true;
        }

        private void customersDataGridView_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (customersDataGridView.Columns[customersDataGridView.CurrentCell.ColumnIndex].Name == "DeleteColumn")
                customersDataGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private void customersDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            // Check if any delete check boxes are checked
            if (customersDataGridView.Columns[e.ColumnIndex].Name == "DeleteColumn")
            {
                int rowIndex = customersDataGridView.CurrentCell.RowIndex;
                if (Convert.ToBoolean(customersDataGridView.Rows[rowIndex].Cells["DeleteColumn"].FormattedValue))
                {
                    ++checkCounts;
                }
                else
                {
                    --checkCounts;
                }

                if (checkCounts != 0)
                {
                    tlbtnDelete.Enabled = true;
                    mnuDeleteCustomer.Enabled = true;
                }
                else
                {
                    tlbtnDelete.Enabled = false;
                    mnuDeleteCustomer.Enabled = false;
                }
            }
            else if (formInitialized)
            {
                SaveChanges(sender, e);
            }
        }

        private void customersDataGridView_MouseLeave(object sender, EventArgs e)
        {
            customersDataGridView.ClearSelection();
            customersDataGridView.CurrentCell = null;
        }

        private void customersDataGridView_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            customersDataGridView.ClearSelection();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        // Save changes for the grid
        private void SaveChanges(object sender, DataGridViewCellEventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to modify this customer?",
                "Confirm Modification",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                this.Validate();
                this.customersBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this._AHDatabase_CustomerContextDataSet);
            }

            // Update grid view display and disable delete button
            FillTableAndDisableButtons();
        }

        // Overload save changes for the save button on the toolbar
        private void SaveChanges(object sender, EventArgs e)
        {
            this.Validate();
            this.customersBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this._AHDatabase_CustomerContextDataSet);
        }

        // Event handler for deleting customers
        private void DeleteCustomers(object sender, EventArgs e)
        {
            DialogResult result;
            if (checkCounts > 1)
            {
                result = MessageBox.Show("Are you sure you want to delete these customers?",
                    "Confirm Delete",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            }
            else
            {
                result = MessageBox.Show("Are you sure you want to delete this customer?",
                    "Confirm Delete",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            }

            if (result == DialogResult.Yes)
            {
                for (int i = customersDataGridView.Rows.Count - 1; i >= 0; --i)
                {
                    if (Convert.ToBoolean(customersDataGridView.Rows[i].Cells["DeleteColumn"].FormattedValue))
                    {
                        GetCustomerDataFromRow(ref customer, i);

                        if (customer != null)
                        {
                            AHEntity.AHCustomers.Customers.Remove(customer);

                            // Disable delete buttons / delete menu button
                            tlbtnDelete.Enabled = false;
                            mnuDeleteCustomer.Enabled = false;

                            // Set checkbox count equal to zero
                            checkCounts = 0;
                        }
                    }
                }
                // Update database
                AHEntity.AHCustomers.SaveChanges();

                // Update datagrid display
                this.customersTableAdapter.Fill(this._AHDatabase_CustomerContextDataSet.Customers);
            }
        }

        // Display customers event handler
        private void DisplayCustomers(object sender, EventArgs e)
        {
            if (splitContainer1.Panel2Collapsed)
            {
                this.Height = 675;
                splitContainer1.SplitterDistance = 155;
            }
            else
            {
                this.Height = 257;
            }

            // Expand/collapse customer list when pressed
            splitContainer1.Panel2Collapsed = !splitContainer1.Panel2Collapsed;

            // Enable searching for customers
            txtSearchBox.Text = "";
            txtSearchBox.Enabled = !txtSearchBox.Enabled;
        }

        // Add customer to list event handler
        private void AddCustomer(object sender, EventArgs e)
        {
            customer = new Customer();

            if (InsertCustomerData(customer))
            {
                try
                {
                    // Add customer to database
                    AHEntity.AHCustomers.Customers.Add(customer);

                    // Save user customer information into entity object
                    AHEntity.AHCustomers.SaveChanges();

                    // Clear text boxes
                    ClearData();

                    MessageBox.Show("Customer Added.", "Successful");

                    // Update data grid display and disable delete buttons
                    FillTableAndDisableButtons();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
        }

        private void GetCustomerDataFromRow(ref Customer selectedCustomer, int i)
        {
            // Cell name corresponds to the column named "Name"
            int ID = Convert.ToInt32(customersDataGridView.Rows[i]
                .Cells["CustID"].Value);

            // LINQ query to get the customer selected in datagridview
            selectedCustomer =
            (from customer in AHEntity.AHCustomers.Customers
                where customer.CustId == ID
                select customer).SingleOrDefault();
        }

        // Clears the data after typing in a customer
        private void ClearData()
        {
            customer = null;
            txtName.Text = "";
            txtDate.Text = "";
            txtEmail.Text = "";
            txtPhoneNumber.Text = "";
            txtName.Focus();
        }

        // Inserts customer data from the text boxes on the form
        private bool InsertCustomerData(Customer customer)
        {
            if (IsValid())
            {
                customer.Name = txtName.Text.Trim();
                customer.Date = Convert.ToDateTime(txtDate.Text.Trim());
                customer.PhoneNumber = txtPhoneNumber.Text.Trim();
                customer.Email = txtEmail.Text.Trim();
                return true;
            }
            return false;
        }

        // Checks if the text entered is valid
        private bool IsValid()
        {
            return Validator.IsPresent(txtName)
                   && Validator.IsPresent(txtDate)
                   && Validator.IsDate(txtDate)
                   && Validator.IsPresent(txtPhoneNumber);
        }

        // updates the table after an operation and disables the delete button
        private void FillTableAndDisableButtons()
        {
            this.customersTableAdapter.Fill(this._AHDatabase_CustomerContextDataSet.Customers);
            tlbtnDelete.Enabled = false;
            mnuDeleteCustomer.Enabled = false;
        }

        private void SearchName(object sender, EventArgs e)
        {
            // Set grid view display datasource
            customersDataGridView.DataSource = _AHDatabase_CustomerContextDataSet.Customers;

            // Filter rows based on search
            (customersDataGridView.DataSource as DataTable)
                .DefaultView.RowFilter = string.Format("Name LIKE '%{0}%'", txtSearchBox.Text);
        }
    }
}


