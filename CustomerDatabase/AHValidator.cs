﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CustomerDatabase
{
    public static class Validator
    { 
        private static string title = "Entry Error";

        public static string Title { get; set; }

        public static bool IsPresent(TextBox txtBox)
        {
            if (txtBox.Text == "")
            {
                MessageBox.Show(txtBox.Tag + " is a required field.", Title);
                txtBox.Focus();
                return false;
            }
            return true;
        }

        public static bool IsEmail(TextBox txtBox)
        {
            string email = txtBox.Text;

            if (!email.Contains("@") || !email.Contains("."))
            {
                MessageBox.Show("Email must be a valid email address.", Title);
                txtBox.Focus();
                return false;
            }
            return true;
        }
        public static bool IsDate(TextBox txtBox)
        {
            DateTime date;
            if (!DateTime.TryParse(txtBox.Text, out date))
            {
                MessageBox.Show("Date format must be valid.", Title);
                txtBox.Focus();
                return false;
            }
            return true;
        }
    }
}
